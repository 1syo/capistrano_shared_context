# CapistranoSharedContext

shared_context for capistrano-spec

## Installation

Add this line to your application's Gemfile:

    gem 'capistrano_shared_context', github: '1syo/capistrano_shared_context', tag: 'v0.0.1'

And then execute:

    $ bundle

## Usage

If you have capistrano recipe like following.

    module Capistrano::CurrentBranch
      def self.load_into(configuration)
        configuration.load do
          def current_branch
            `git symbolic-ref --short HEAD`.chomp
          end
        end
      end
    end

    if Capistrano::Configuration.instance
      Capistrano::CurrentBranch.load_into(Capistrano::Configuration.instance)
    end

Then you can write following.

    describe Capistrano::CurrentBranch do
      include_context "capistrano"

      before do
        Capistrano::CurrentBranch.load_into(subject)
      end

      describe "#current_branch" do
        let(:branch) { "master" }
        before do
          Object.any_instance.stub(:`).with("git symbolic-ref --short HEAD") { branch }
        end

        its(:current_branch) { should eq branch }
      end
    end

## Contributing

1. Fork it
2. Create your feature branch (`git checkout -b my-new-feature`)
3. Commit your changes (`git commit -am 'Add some feature'`)
4. Push to the branch (`git push origin my-new-feature`)
5. Create new Pull Request
