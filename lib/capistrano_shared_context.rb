require "capistrano_shared_context/version"
require "capistrano"

require "rspec/core"
shared_context 'capistrano' do
  let(:configuration) { Capistrano::Configuration.new }
  subject { configuration }

  before do
    configuration.extend(Capistrano::Spec::ConfigurationExtension)
  end
end
